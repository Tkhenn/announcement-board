using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace AnnouncementBoard.Storage.EF;

[Index(nameof(Name), IsUnique = true)]
public class Tag
{
    public Tag()
    {
        Announcements = new HashSet<Announcement>();
    }

    [Key] public int Id { get; set; }
    [MaxLength(50)] public string Name { get; set; } = null!;
    public ICollection<Announcement> Announcements { get; set; }
}