using System.ComponentModel.DataAnnotations;

namespace AnnouncementBoard.Storage.EF;

public class Announcement
{
    public Announcement()
    {
        Tags = new HashSet<Tag>();
    }

    [Key] public int Id { get; set; }
    [MaxLength(100)] public string Author { get; set; } = null!;
    [MaxLength(int.MaxValue)] public string Content { get; set; } = null!;
    public ICollection<Tag> Tags { get; set; }
}