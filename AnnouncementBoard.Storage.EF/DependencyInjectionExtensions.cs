using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AnnouncementBoard.Storage.EF;

public static class DependencyInjectionExtensions
{
    public static void AddDatabaseBoardRepository(this IServiceCollection services, string connectionString)
    {
        services.AddDbContextFactory<BoardDbContext>(
            optionsBuilder => optionsBuilder.UseSqlite(connectionString));
        services.AddTransient<IBoardRepository, DatabaseBoardRepository>();
    }

    public static void InitializeDatabase(this IServiceProvider services)
    {
        var contextFactory = services.GetService<IDbContextFactory<BoardDbContext>>();
        if (contextFactory is null) return;
        using var db = contextFactory.CreateDbContext();
        db.Database.Migrate();
    }
}