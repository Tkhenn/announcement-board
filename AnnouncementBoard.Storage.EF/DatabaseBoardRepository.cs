using Microsoft.EntityFrameworkCore;

namespace AnnouncementBoard.Storage.EF;

public class DatabaseBoardRepository : IBoardRepository
{
    private readonly IDbContextFactory<BoardDbContext> _contextFactory;

    public DatabaseBoardRepository(IDbContextFactory<BoardDbContext> contextFactory)
    {
        _contextFactory = contextFactory;
    }

    public async Task<int> AddAnnouncement(Entities.Announcement announcement)
    {
        await using var db = await _contextFactory.CreateDbContextAsync();
        var entity = new Announcement
        {
            Author = announcement.Author,
            Content = announcement.Content
        };
        foreach (var tag in announcement.Tags) entity.Tags.Add(await GetOrCreateTag(db, tag));
        await db.Announcement.AddAsync(entity);
        await db.SaveChangesAsync();
        return entity.Id;
    }

    public async Task<IReadOnlyCollection<Entities.Announcement>> GetAnnouncementsByTags(IReadOnlyCollection<string> tags)
    {
        await using var db = await _contextFactory.CreateDbContextAsync();
        var entities = await db.Announcement.Where(a => a.Tags.Any(tag => tags.Contains(tag.Name)))
            .Select(a => new { a.Id, a.Author, a.Content, Tags = a.Tags.Select(t => t.Name) }).ToListAsync();
        return entities.Select(e => new Entities.Announcement(e.Id, e.Author, e.Content, e.Tags)).ToList();
    }

    private static async Task<Tag> GetOrCreateTag(BoardDbContext db, string name)
    {
        var tag = await db.Tag.FirstOrDefaultAsync(t => t.Name == name);
        if (tag != null) return tag;
        tag = new()
        {
            Name = name
        };
        await db.Tag.AddAsync(tag);
        return tag;
    }
}