﻿using Microsoft.EntityFrameworkCore;

namespace AnnouncementBoard.Storage.EF;

public class BoardDbContext : DbContext
{
    public DbSet<Announcement> Announcement { get; set; } = null!;
    public DbSet<Tag> Tag { get; set; } = null!;

    public BoardDbContext(DbContextOptions<BoardDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Announcement>().HasMany(a => a.Tags).WithMany(t => t.Announcements);
    }
}