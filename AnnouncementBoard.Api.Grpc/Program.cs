using AnnouncementBoard.Api.Grpc.Services;
using AnnouncementBoard.Services;
using AnnouncementBoard.Storage.EF;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDatabaseBoardRepository(builder.Configuration.GetConnectionString("Board"));
//builder.Services.AddMemoryBoardRepository();
builder.Services.AddBoardService();

builder.Services.AddGrpc();

var app = builder.Build();

// Configure the HTTP request pipeline.
app.MapGrpcService<AnnouncementBoardService>();
app.MapGet("/",
    () =>
        "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

// Auto-migrations
app.Services.InitializeDatabase();

app.Run();