using AnnouncementBoard.Services;
using Grpc.Core;

namespace AnnouncementBoard.Api.Grpc.Services;

public class AnnouncementBoardService : AnnouncementBoard.AnnouncementBoardBase
{
    private readonly IBoardService _boardService;

    public AnnouncementBoardService(IBoardService boardService)
    {
        _boardService = boardService;
    }

    public override async Task<AddResponse> Add(AddRequest request, ServerCallContext context)
    {
        var announcement = Entities.Announcement.Create(request.Author, request.Content, request.Tags);
        var id = await _boardService.AddAnnouncement(announcement);
        return new()
        {
            Id = id
        };
    }

    public override async Task<GetByTagsResponse> GetByTags(GetByTagsRequest request, ServerCallContext context)
    {
        var announcements = await _boardService.GetAnnouncementsByTags(request.Tags);
        return new()
        {
            Announcements =
            {
                announcements.Select(a => new Announcement
                    { Id = a.Id, Author = a.Author, Content = a.Content, Tags = { a.Tags } })
            }
        };
    }
}