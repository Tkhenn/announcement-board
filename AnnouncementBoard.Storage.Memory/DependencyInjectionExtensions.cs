using Microsoft.Extensions.DependencyInjection;

namespace AnnouncementBoard.Storage.Memory;

public static class DependencyInjectionExtensions
{
    public static void AddMemoryBoardRepository(this IServiceCollection services)
    {
        services.AddSingleton<IBoardRepository, MemoryBoardRepository>();
    }
}