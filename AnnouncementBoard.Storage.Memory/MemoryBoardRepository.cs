﻿using System.Collections.Concurrent;
using AnnouncementBoard.Entities;

namespace AnnouncementBoard.Storage.Memory;

public class MemoryBoardRepository : IBoardRepository
{
    private readonly ConcurrentBag<Announcement> _announcements = new();
    private volatile int _nextId;

    public Task<int> AddAnnouncement(Announcement announcement)
    {
        var id = Interlocked.Increment(ref _nextId);
        _announcements.Add(Announcement.Create(id, announcement));
        return Task.FromResult(id);
    }

    public Task<IReadOnlyCollection<Announcement>> GetAnnouncementsByTags(IReadOnlyCollection<string> tags)
    {
        var result = _announcements.Where(a => tags.Any(tag => a.Tags.Contains(tag))).ToList();
        return Task.FromResult<IReadOnlyCollection<Announcement>>(result);
    }
}