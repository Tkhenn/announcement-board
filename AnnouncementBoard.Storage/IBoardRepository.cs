﻿using AnnouncementBoard.Entities;

namespace AnnouncementBoard.Storage;

public interface IBoardRepository
{
    Task<int> AddAnnouncement(Announcement announcement);
    Task<IReadOnlyCollection<Announcement>> GetAnnouncementsByTags(IReadOnlyCollection<string> tags);
}