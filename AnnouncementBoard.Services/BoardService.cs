using AnnouncementBoard.Entities;
using AnnouncementBoard.Services.Validators;
using AnnouncementBoard.Storage;
using FluentValidation;

namespace AnnouncementBoard.Services;

public class BoardService : IBoardService
{
    private readonly IBoardRepository _boardRepository;
    private readonly AnnouncementValidator _announcementValidator;

    public BoardService(IBoardRepository boardRepository, AnnouncementValidator announcementValidator)
    {
        _boardRepository = boardRepository;
        _announcementValidator = announcementValidator;
    }

    public async Task<int> AddAnnouncement(Announcement announcement)
    {
        await _announcementValidator.ValidateAndThrowAsync(announcement);
        return await _boardRepository.AddAnnouncement(announcement);
    }

    public async Task<IReadOnlyCollection<Announcement>> GetAnnouncementsByTags(IReadOnlyCollection<string> tags)
    {
        if (tags.Count == 0)
        {
            throw new ArgumentException("Must be at least one tag for search", nameof(tags));
        }

        return await _boardRepository.GetAnnouncementsByTags(tags);
    }
}