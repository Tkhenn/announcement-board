﻿using AnnouncementBoard.Entities;

namespace AnnouncementBoard.Services;

public interface IBoardService
{
    Task<int> AddAnnouncement(Announcement announcement);
    Task<IReadOnlyCollection<Announcement>> GetAnnouncementsByTags(IReadOnlyCollection<string> tags);
}