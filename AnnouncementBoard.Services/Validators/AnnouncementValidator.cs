using AnnouncementBoard.Entities;
using FluentValidation;

namespace AnnouncementBoard.Services.Validators;

public class AnnouncementValidator : AbstractValidator<Announcement>
{
    public AnnouncementValidator()
    {
        RuleFor(a => a.Author).NotEmpty().MaximumLength(100);
        RuleFor(a => a.Content).NotEmpty();
        RuleForEach(a => a.Tags).NotEmpty().MaximumLength(50);
    }
}