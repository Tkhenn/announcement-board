using AnnouncementBoard.Services.Validators;
using Microsoft.Extensions.DependencyInjection;

namespace AnnouncementBoard.Services;

public static class DependencyInjectionExtensions
{
    public static void AddBoardService(this IServiceCollection services)
    {
        services.AddTransient<AnnouncementValidator>();
        services.AddTransient<IBoardService, BoardService>();
    }
}