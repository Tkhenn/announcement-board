using System.Threading.Tasks;
using AnnouncementBoard.Entities;
using AnnouncementBoard.Storage.Memory;
using NUnit.Framework;

namespace AnnouncementBoard.Storage.Tests;

public class MemoryBoardRepositoryTests
{
    private IBoardRepository _boardRepository = CreateBoardRepository();

    [SetUp]
    public void Setup()
    {
        _boardRepository = CreateBoardRepository();
    }

    private static IBoardRepository CreateBoardRepository()
    {
        var boardRepository = new MemoryBoardRepository();
        boardRepository.AddAnnouncement(Announcement.Create("Aleksey", "Test announcement 1",
            new[] { "Private", "Deals" }));
        boardRepository.AddAnnouncement(Announcement.Create("Aleksey", "Test announcement 2",
            new[] { "Private", "House" }));
        boardRepository.AddAnnouncement(Announcement.Create("Aleksey", "Test announcement 3",
            new[] { "Clothes" }));
        boardRepository.AddAnnouncement(Announcement.Create("Aleksey", "Test announcement 4",
            new[] { "Clothes", "House" }));
        return boardRepository;
    }

    [Test]
    public void NewId()
    {
        var id = _boardRepository.AddAnnouncement(Announcement.Create("Aleksey", "Test announcement 5",
            new[] { "Deals", "House" }));
        Assert.AreNotEqual(0, id);
    }

    [Test]
    public async Task GrowCount()
    {
        var oldList = await _boardRepository.GetAnnouncementsByTags(new[] { "Deals" });
        await _boardRepository.AddAnnouncement(Announcement.Create("Aleksey", "Test announcement 5",
            new[] { "Deals", "House" }));
        var newList = await _boardRepository.GetAnnouncementsByTags(new[] { "Deals" });
        Assert.Greater(newList.Count, oldList.Count, "Count of announcements by tag");
    }

    [TestCase("Private", 2)]
    [TestCase("Deals", 1)]
    [TestCase("House", 2)]
    [TestCase("Clothes", 2)]
    [TestCase("Something", 0)]
    public async Task CountByTag(string tag, int count)
    {
        var list = await _boardRepository.GetAnnouncementsByTags(new[] { tag });
        Assert.AreEqual(count, list.Count, "Count of announcements by tag");
    }

    [TestCase(new[] { "Private", "House" }, 3)]
    [TestCase(new[] { "Deals", "Clothes" }, 3)]
    [TestCase(new[] { "Something" }, 0)]
    public async Task CountByTags(string[] tags, int count)
    {
        var list = await _boardRepository.GetAnnouncementsByTags(tags);
        Assert.AreEqual(count, list.Count, "Count of announcements by tags");
    }
}