using AnnouncementBoard.Entities;
using AnnouncementBoard.Storage.Memory;
using FluentValidation;
using NUnit.Framework;

namespace AnnouncementBoard.Services.Tests;

public class BoardServiceTests
{
    private IBoardService _boardService = CreateBoardService();

    [SetUp]
    public void Setup()
    {
        _boardService = CreateBoardService();
    }

    private static IBoardService CreateBoardService()
    {
        return new BoardService(new MemoryBoardRepository(), new());
    }

    [TestCase(null)]
    [TestCase("")]
    [TestCase("   ")]
    public void AddEmptyAuthor(string author)
    {
        Assert.That(() => _boardService.AddAnnouncement(Announcement.Create(author, "Test", new[] { "Test" })),
            Throws.Exception.TypeOf(typeof(ValidationException)));
    }

    [TestCase(null)]
    [TestCase("")]
    [TestCase("   ")]
    public void AddEmptyContent(string content)
    {
        Assert.That(() => _boardService.AddAnnouncement(Announcement.Create("Test", content, new[] { "Test" })),
            Throws.Exception.TypeOf(typeof(ValidationException)));
    }

    [TestCase(null)]
    [TestCase("")]
    [TestCase("   ")]
    public void AddEmptyTag(string tag)
    {
        Assert.That(() => _boardService.AddAnnouncement(Announcement.Create("Test", "Test", new[] { tag })),
            Throws.Exception.TypeOf(typeof(ValidationException)));
    }
}