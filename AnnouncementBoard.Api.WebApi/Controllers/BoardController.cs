using AnnouncementBoard.Api.WebApi.Entities;
using AnnouncementBoard.Entities;
using AnnouncementBoard.Services;
using Microsoft.AspNetCore.Mvc;

namespace AnnouncementBoard.Api.WebApi.Controllers;

/// <summary>
/// Announcement board
/// </summary>
[ApiController]
[Route("board")]
public class BoardController : ControllerBase
{
    private readonly IBoardService _boardService;

    public BoardController(IBoardService boardService)
    {
        _boardService = boardService;
    }

    /// <summary>
    /// Add new announcement to board
    /// </summary>
    /// <param name="data">Announcement data</param>
    /// <returns>Announcement id</returns>
    [HttpPost]
    public async Task<int> Add(AnnouncementCreateData data)
    {
        var announcement = Announcement.Create(data.Author, data.Content, data.Tags);
        return await _boardService.AddAnnouncement(announcement);
    }

    /// <summary>
    /// Search announcements by tag
    /// </summary>
    /// <param name="tag">Tag for searching</param>
    /// <returns>List of found announcements</returns>
    [HttpGet("tag/{tag}")]
    public async Task<IEnumerable<AnnouncementData>> GetByTag(string? tag)
    {
        return await GetByTags(new[] { tag ?? "" });
    }

    /// <summary>
    /// Search announcements by tags
    /// </summary>
    /// <param name="tags">List of tags for searching</param>
    /// <returns>List of found announcements</returns>
    [HttpPost("tag")]
    public async Task<IEnumerable<AnnouncementData>> GetByTags(IReadOnlyCollection<string>? tags)
    {
        var announcements = await _boardService.GetAnnouncementsByTags(tags ?? Array.Empty<string>());
        return announcements.Select(a => new AnnouncementData(a.Id, a.Author, a.Content, a.Tags.ToArray()));
    }
}