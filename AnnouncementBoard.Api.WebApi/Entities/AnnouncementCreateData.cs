namespace AnnouncementBoard.Api.WebApi.Entities;

/// <summary>
/// Data for create new announcement
/// </summary>
public class AnnouncementCreateData
{
    /// <summary>
    /// Author
    /// </summary>
    public string? Author { get; set; }
    /// <summary>
    /// Announcement content
    /// </summary>
    public string? Content { get; set; }
    /// <summary>
    /// List of tags associated with announcement
    /// </summary>
    public string?[]? Tags { get; set; }
}