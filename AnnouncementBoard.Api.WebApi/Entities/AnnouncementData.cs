namespace AnnouncementBoard.Api.WebApi.Entities;

/// <summary>
/// Data of announcement
/// </summary>
public class AnnouncementData
{
    /// <summary>
    /// Announcement id
    /// </summary>
    public int Id { get; set; }
    /// <summary>
    /// Author
    /// </summary>
    public string Author { get; set; }
    /// <summary>
    /// Announcement content
    /// </summary>
    public string Content { get; set; }
    /// <summary>
    /// List of tags associated with announcement
    /// </summary>
    public string[] Tags { get; set; }

    public AnnouncementData(int id, string author, string content, string[] tags)
    {
        Id = id;
        Author = author;
        Content = content;
        Tags = tags;
    }
}