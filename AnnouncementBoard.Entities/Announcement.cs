﻿namespace AnnouncementBoard.Entities;

public class Announcement
{
    public int Id { get; }
    public string Author { get; }
    public string Content { get; }
    public IReadOnlyCollection<string> Tags { get; }

    public Announcement(int id, string? author, string? content, IEnumerable<string?>? tags)
    {
        Id = id;
        Author = author ?? "";
        Content = content ?? "";
        Tags = tags?.Select(t => t ?? "").ToList() ?? new List<string>();
    }

    public static Announcement Create(string? author, string? content, IEnumerable<string?>? tags)
    {
        return new(0, author, content, tags);
    }

    public static Announcement Create(int id, Announcement announcement)
    {
        return new(id, announcement.Author, announcement.Content, announcement.Tags);
    }
}